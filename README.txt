This module provides a form API element type that you can use in your own
programatically generated drupal forms.  This is not a field for the drupal
field system.  It has no submit handler, no validation, and no storage -- these are
all up to you.  What it does do is allow you to wrap a form element of a given
type in a wrapper with an add more button, which when clicked adds another
identical element and rebuilds the form to allow checksums and validation to still work.

The included addmore_example module shows you how to use it.
Here's a yet simpler example form:

<code>
function mymodule_form($form, $form_state, $no_js_use = FALSE) {
    $form = array();
    $form['#tree'] = TRUE;

    /// here is the addmore element, it's simply #type => 'addmore' and #subfield_definition => your other element definition array ... easy peasy
   /// and optionally, set the  #max_ #min_ and #initial_ subfields properties
    $form['addtextstuff'] = array(
        '#type' => 'addmore',
        '#title' => t('Your Favorite Teams'),
        '#description' => t("put your description here maybe?"),
        '#subfield_definition' => array (  // put the other form element definition you want right here as #subfield
                  '#type' => 'textfield',
                  '#title' => t('Team name'),
                  '#description' => t('putting a description under every one of the subfields is too cluttered'),
        ),
        '#submit_value' => t('Add another one?'),
        '#initial_subfields' => 3,
        '#max_subfields' => 4,
        '#min_subfields' => 2,  // if not set, this is set to the value of #initial_subfields

    );
   ///// Get it?  Easy.

    $form['submit'] = array(  // This is just your normal submit button, part of your normal form.
        '#type' => 'submit',
        '#value' => t('Submit'),
    );
    return $form;
}
</code>


This module was produced by wegewerk gmbh
Contributed by Amnesty International Germany

Many thanks to the folks who worked on documenting how to do this:
nehajn: https://www.drupal.org/node/2291745
https://api.drupal.org/api/examples/ajax_example!ajax_example_graceful_degradation.inc/function/ajax_example_add_more/7
